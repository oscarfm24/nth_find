import React, { createContext, useState } from "react";
import { n_th } from "../features/getNth";

const AppContext = createContext();

function AppProvider(props) {
  const [n, setN] = useState("");
  const [correct, setCorrect] = useState(false);
  const [serie, setSerie] = useState("1");
  const [result, setResult] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    if (n) {
      switch (serie) {
        case "1":
          try {
            let r = n_th.primeNumber(parseInt(n));
            console.info(r);

            if (typeof r === "number") {
              setCorrect(true);
              setResult(r);
            } else {
              setCorrect(false);
            }
          } catch (error) {
            console.error("Ups an error occurred" + error.message);
          }
          break;

        case "2":
          try {
            let r = n_th.multiplesOfThree(parseInt(n));
            console.info(r);

            if (typeof r === "number") {
              setCorrect(true);
              setResult(r);
            } else {
              setCorrect(false);
            }
          } catch (error) {
            console.error("Ups an error occurred" + error.message);
          }
          break;

        default:
          throw new TypeError("The selected option is not valid ☹️");
      }
    } else {
      console.error("Enter n");
      setCorrect(false);
    }
  };

  return (
    <AppContext.Provider
      value={{
        n,
        setN,
        handleSubmit,
        correct,
        setCorrect,
        serie,
        setSerie,
        result,
        setResult,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
}

export { AppContext, AppProvider };
