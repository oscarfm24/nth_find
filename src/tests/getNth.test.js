import { n_th } from "../features/getNth";

//? n-enésimo termino de la serie de los numero primos: [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
//? n-enésimo termino de la serie de los múltiplos de 3: [0, 3, 6, 9, 12, 15, 18, 21, 24, 27]

describe("Métodos <<primeNumber>> & <<multiplesOfThree>> de la clase getNth", () => {
  describe("Método: Retornar el enésimo de los números primos, dependiendo del valor n", () => {
    describe("n_th", () => {
      test("Retornar 2; 2 es el primer numero en la posición 1 de la serie de los números primos", () => {
        const result = n_th.primeNumber(1);
        expect(result).toBe(2);
      });
      test("Retornar 3; 3 es el primer numero en la posición 2 de la serie de los números primos", () => {
        const result = n_th.primeNumber(2);
        expect(result).toBe(3);
      });
      test("Retornar 5; 5 es el primer numero en la posición 3 de la serie de los números primos", () => {
        const result = n_th.primeNumber(3);
        expect(result).toBe(5);
      });
      test("Retornar 7; 7 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.primeNumber(4);
        expect(result).toBe(7);
      });
      test("Retornar 11; 11 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.primeNumber(5);
        expect(result).toBe(11);
      });
      test("Retornar 13; 13 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.primeNumber(6);
        expect(result).toBe(13);
      });
      test("Retornar 17; 17 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.primeNumber(7);
        expect(result).toBe(17);
      });
      test("Retornar 19; 19 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.primeNumber(8);
        expect(result).toBe(19);
      });
      test("Retornar 23; 23 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.primeNumber(9);
        expect(result).toBe(23);
      });
    });
  });

  describe("Método: Retornar el enésimo de los múltiplos de 3, dependiendo del valor n", () => {
    describe("n_th", () => {
      test("Retornar 0; 0 es el primer numero en la posición 1 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(1);
        expect(result).toBe(0);
      });
      test("Retornar 3; 3 es el primer numero en la posición 2 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(2);
        expect(result).toBe(3);
      });
      test("Retornar 6; 6 es el primer numero en la posición 3 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(3);
        expect(result).toBe(6);
      });
      test("Retornar 9; 9 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(4);
        expect(result).toBe(9);
      });
      test("Retornar 12; 12 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(5);
        expect(result).toBe(12);
      });
      test("Retornar 15; 15 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(6);
        expect(result).toBe(15);
      });
      test("Retornar 18; 18 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(7);
        expect(result).toBe(18);
      });
      test("Retornar 21; 21 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(8);
        expect(result).toBe(21);
      });
      test("Retornar 24; 24 es el primer numero en la posición 4 de la serie de los números primos", () => {
        const result = n_th.multiplesOfThree(9);
        expect(result).toBe(24);
      });
    });
  });
});
