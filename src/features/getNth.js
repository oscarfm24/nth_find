class getNth {
  //constructor() {}

  isPrime(i) {
    if (i <= 1) return false;

    if (i === 2 || i === 3) return true;

    if (i % 2 === 0 || i % 3 === 0) return false;

    for (let index = 5; index * index <= i; index = index + 6)
      if (i % index === 0 || i % (index + 2) === 0) return false;

    return true;
  }

  primeNumber(n) {
    let i = 2;

    if ((typeof n !== "number" && !Number.isInteger(n)) || n === 0)
      throw new TypeError("The number entered is not correct☹️");

    while (n > 0) {
      this.isPrime(i) && n--;
      i++;
    }

    i -= 1;
    return i;
  }

  multiplesOfThree(n) {
    if ((typeof n !== "number" && !Number.isInteger(n)) || n === 0)
      throw new TypeError("The number entered is not correct☹️");

    return 3 * n - 3;
  }
}

const n_th = new getNth();

export { n_th };
