import React, { useContext } from "react";
import { AppContext } from "../context";

function Form() {
  const { handleSubmit, n, setN, serie, setSerie, setResult, setCorrect } =
    useContext(AppContext);

  const onChange = (event) => {
    setN(event.target.value);
  };

  const keyUp = (e) => {
    let value = e.target.value;

    if (value === "") {
      setResult("");
      setCorrect(false);
    }
  };

  const onChangeSerie = (e) => {
    setSerie(e.target.value);
  };

  return (
    <section className="container mx-auto flex flex-col items-center">
      <h1 className="text-2xl text-gray-900 font-semibold my-5">
        Bienvenido a esta pequeña aplicación
      </h1>
      <form onSubmit={handleSubmit}>
        <label className="block" htmlFor="n">
          <span className="block text-lg text-gray-600 font-semibold">
            Ingresa en ingrese el valor <b>n</b>
          </span>
          <input
            className="block shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline text-xs text-gray-600 font-semibold my-3"
            id="n"
            name="n"
            value={n}
            onChange={onChange}
            onKeyUp={keyUp}
            type="number"
            min="1"
            placeholder="1"
            required
          />
        </label>
        <label className="block my-6" htmlFor="serie">
          <span className="block text-lg text-gray-600 font-semibold">
            Selecciona una serie
          </span>
          <select
            className="block shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline text-xs text-gray-600 font-semibold my-3"
            id="serie"
            name="serie"
            value={serie}
            onChange={onChangeSerie}
          >
            <option value="1">Números primos</option>
            <option value="2">Múltiplos de 3</option>
          </select>
        </label>
        <button
          className="bg-sky-600 w-full rounded py-2 text-white"
          type="submit"
        >
          Obtener en termino{" "}
          <code className="shadow p-1 font-semibold text-xs">n-esimo</code>
        </button>
      </form>
    </section>
  );
}

export { Form };
