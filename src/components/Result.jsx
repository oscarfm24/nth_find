import React, { useContext } from "react";
import { AppContext } from "../context";

function Result() {
  const { correct, result } = useContext(AppContext);

  let show = 0;

  if (result && correct === false) show = 1;

  return (
    <section className="mt-10">
      <span
        className={`text-red-400 font-semibold text-sm ${!show && "hidden"}`}
      >
        Por favor ingresa un numero valido
      </span>
      <h1
        className={`text-2xl font-semibold text-gray-700 ${
          correct === false && "hidden"
        }`}
      >
        El resultado es: {`${result}`}
      </h1>
    </section>
  );
}

export { Result };
