import React from "react";
import { AppProvider } from "../context";
import { Form } from "./Form";
import { Result } from "./Result";

function App() {
  return (
    <AppProvider>
      <main className="p-5 w-full flex flex-col justify-between items-center">
        <Form />
        <Result />
      </main>
    </AppProvider>
  );
}

export { App };
